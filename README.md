# Monitoring Yucca

## Базовый мониторинг Yucca и инстанса на базе **prometheus** и **grafana**

![](main.png)

## Установка

Скопируйте этот репозиторий на хост с yucca или другой инстанс, где планируете развернуть мониторинг  

```sh
git clone https://gitlab.com/yuccastream/public/monitoring.git
cd monitoring
```

### Зависимости

Убедитесь что установлены все зависимости

- [docker](https://docs.docker.com/engine/install/)
- [docker-compose](https://docs.docker.com/compose/install/)

### Запуск

```sh
docker compose up -d
```

### Остановка и удаление

```sh
docker compose down -v
```

### Использование

После запуска grafana будет доступна на `3000` порту инстанса, реквизиты по умолчанию:

- login: `admin`
- password: `Yucca`
